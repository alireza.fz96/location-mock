import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {
    path: 'public-addresses',
    loadChildren: () =>
      import('./modules/public-addresses/public-addresses.module').then(
        (m) => m.PublicAddressesModule
      ),
  },
  {
    canActivate: [AuthGuard],
    path: 'favorite-addresses',
    loadChildren: () =>
      import('./modules/favorite-addresses/favorite-addresses.module').then(
        (m) => m.FavoriteAddressesModule
      ),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./modules/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: '**',
    redirectTo: 'public-addresses',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
