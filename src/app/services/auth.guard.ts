import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.authService.user.pipe(
      map((user) => {
        const isAuthenticated = !!user;
        if (isAuthenticated) return true;
        else {
          this.router.navigate(['/auth']);
          return false;
        }
      })
    );
  }
}
