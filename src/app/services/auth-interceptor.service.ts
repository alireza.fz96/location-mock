import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const user = this.authService.user.getValue();
    if (!user) return next.handle(req);
    const headers = req.headers.set('Authorization', user.token);
    const modifiedReq = req.clone({ headers });
    return next.handle(modifiedReq);
  }
}
