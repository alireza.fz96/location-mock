import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { ResponseData } from '../models/response-data.model';
import { ParsedToken } from '../models/parsed-token.model';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthService {
  user = new BehaviorSubject<User>(null);
  private expirationTimerId: number;
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private router: Router) {}

  login(email: string, password: string): Observable<User> {
    return this.http
      .post<ResponseData>(this.baseUrl + '/login', {
        email,
        password,
      })
      .pipe(map((res) => this.handleAuth(res)));
  }

  signup(email: string, password: string): Observable<User> {
    return this.http
      .post<ResponseData>(this.baseUrl + '/signup', {
        email,
        password,
      })
      .pipe(map((res) => this.handleAuth(res)));
  }

  private handleAuth(res: ResponseData): User {
    const parsedToken = this.parseJwt(res.accessToken);
    const user = this.createUser(parsedToken, res.accessToken);
    this.propagateUser(user);
    this.storeUser(user);
    this.autoLogout(user.expirationDate.getTime() - Date.now());
    return user;
  }

  storeUser(user: User): void {
    localStorage.setItem('user', JSON.stringify(user));
  }

  loadUser(): User {
    const userData: {
      email: string;
      id: string;
      _token: string;
      expirationDate: string;
    } = JSON.parse(localStorage.getItem('user'));
    let loadedUser = null;
    if (userData) {
      const expirationDate = new Date(userData.expirationDate);
      loadedUser = new User(
        userData.email,
        userData.id,
        userData._token,
        expirationDate
      );
    }
    return loadedUser;
  }

  removeUser(): void {
    this.propagateUser(null);
    localStorage.removeItem('user');
  }

  logout(): void {
    this.removeUser();
    clearTimeout(this.expirationTimerId);
    this.router.navigate(['/']);
  }

  autoLogin(): void {
    const loadedUser = this.loadUser();
    if (loadedUser && loadedUser.token) {
      this.user.next(loadedUser);
      this.autoLogout(loadedUser.expirationDate.getTime() - Date.now());
    }
  }

  autoLogout(expirationTime: number): void {
    this.expirationTimerId = setTimeout(() => this.logout(), expirationTime);
  }

  propagateUser(nextUser: User): void {
    this.user.next(nextUser);
  }

  private createUser(pasedToken: ParsedToken, token: string): User {
    const expirationDate = new Date(pasedToken.exp * 1000);
    const user = new User(
      pasedToken.email,
      pasedToken.sub,
      token,
      expirationDate
    );
    return user;
  }

  private parseJwt(token: string): ParsedToken {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(
      atob(base64)
        .split('')
        .map(function (c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join('')
    );
    return JSON.parse(jsonPayload);
  }

  updateUser(email: string, password: string): Observable<User> {
    const userId = this.user.getValue().id;
    return this.http.patch<User>(this.baseUrl + '/users/' + userId, {
      email,
      password,
    });
  }
}
