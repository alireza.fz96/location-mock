import { NgModule } from '@angular/core';
import { AddressDetailsComponent } from './components/address-details/address-details.component';
import { AddressListComponent } from './components/address-list/address-list.component';
import { AddressStartComponent } from './components/address-start/address-start.component';
import { AddLocationComponent } from './components/add-location/add-location.component';
import { AddressLocationComponent } from './components/address-location/address-location.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AddressDetailsComponent,
    AddressListComponent,
    AddressStartComponent,
    AddLocationComponent,
    AddressLocationComponent,
  ],
  imports: [SharedModule, RouterModule],
  exports: [
    AddressDetailsComponent,
    AddressListComponent,
    AddressStartComponent,
    AddLocationComponent,
    SharedModule,
  ],
})
export class SharedAddressesModule {}
