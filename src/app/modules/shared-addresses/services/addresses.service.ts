import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Address } from '../models/address.model';
import { Subject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AddressesService {
  addressesChanged = new Subject<void>();
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient, private auth: AuthService) {}

  fetchAddresses(): Observable<Address[]> {
    return this.http.get<Address[]>(this.baseUrl + '/public-addresses');
  }

  fetchFavoriteAddresses(): Observable<Address[]> {
    const userId = this.auth.user.getValue().id;
    return this.http.get<Address[]>(this.baseUrl + '/favorite-addresses', {
      params: new HttpParams().set('userId', userId + ''),
    });
  }

  fetchFavoriteAddressById(id: number): Observable<Address> {
    return this.http.get<Address>(this.baseUrl + '/favorite-addresses/' + id);
  }

  fetchAddressById(id: number): Observable<Address> {
    return this.http.get<Address>(this.baseUrl + '/public-addresses/' + id);
  }

  fetchAddressesByIds(ids: string): Observable<Address[]> {
    return this.http.get<Address[]>(this.baseUrl + '/public-addresses?' + ids);
  }

  addAddress(address: Address): Observable<Address> {
    delete address.id;
    return this.http
      .post<Address>(this.baseUrl + '/public-addresses', address)
      .pipe(tap(() => this.addressesChanged.next()));
  }

  updateAddress(address: Address): Observable<Address> {
    return this.http
      .put<Address>(this.baseUrl + '/public-addresses/' + address.id, address)
      .pipe(tap(() => this.addressesChanged.next()));
  }

  updateFavoriteAddress(address: Address): Observable<Address> {
    const userId = this.auth.user.getValue().id;
    const body = {
      userId,
      ...address,
    };
    return this.http
      .put<Address>(this.baseUrl + '/favorite-addresses/' + address.id, body)
      .pipe(tap(() => this.addressesChanged.next()));
  }

  addToFavorite(address: Address): Observable<Address> {
    const userId = this.auth.user.getValue().id;
    const body = {
      userId,
      ...address,
    };
    delete body.id;
    return this.http.post<Address>(this.baseUrl + `/favorite-addresses`, body);
  }

  removeAddress(addressId: number): Observable<any> {
    return this.http
      .delete(this.baseUrl + '/public-addresses/' + addressId)
      .pipe(tap(() => this.addressesChanged.next()));
  }

  removeFromFavorite(addressId: number): Observable<any> {
    return this.http
      .delete(this.baseUrl + '/favorite-addresses/' + addressId)
      .pipe(tap(() => this.addressesChanged.next()));
  }
}
