import { Component, OnInit } from '@angular/core';
import { Address } from '../../models/address.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AddressesService } from '../../services/addresses.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.scss'],
})
export class AddressDetailsComponent implements OnInit {
  address: Address;
  isFavorite: boolean;
  isAuthenticated: boolean;

  constructor(
    private route: ActivatedRoute,
    private addressesService: AddressesService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => this.fetchAddress(params));
    this.isAuthenticated = !!this.authService.user.getValue();
  }

  fetchAddress(params: Params) {
    const { id } = params;
    const { isFavorite } = this.route.snapshot.data;
    this.isFavorite = isFavorite;
    const obser = isFavorite
      ? this.addressesService.fetchFavoriteAddressById(+id)
      : this.addressesService.fetchAddressById(+id);
    obser.subscribe((address) => {
      this.address = address;
    });
  }

  onAddToFavorite() {
    this.addressesService
      .addToFavorite(this.address)
      .subscribe((data) => console.log(data));
  }

  onRemove() {
    const obser = this.isFavorite
      ? this.addressesService.removeFromFavorite(this.address.id)
      : this.addressesService.removeAddress(this.address.id);

    obser.subscribe(() => this.onSuccess());
  }

  onSetMarker(marker) {
    
  }

  private onSuccess() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
