import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { Coordinate } from '../../models/coordinate.model';

declare var Mapp: any;

@Component({
  selector: 'app-address-location',
  templateUrl: './address-location.component.html',
  styleUrls: ['./address-location.component.scss'],
})
export class AddressLocationComponent implements OnInit, OnChanges {
  @Input() coordinate: Array<number>;
  @Input() addMode: boolean;
  @Output() setCordinate = new EventEmitter<Coordinate>();
  @Output() setMarker = new EventEmitter<any>();
  map: any;

  constructor() {}

  ngOnInit(): void {
    this.initMap();
    this.showMap();
  }

  ngOnChanges() {
    if (this.map) this.showMap();
  }

  showMap() {
    if (this.addMode) this.addMarker();
    else this.showMarker();
  }

  addMarker() {
    const marker = this.map.addMarker({
      name: 'advanced-marker',
      latlng: {
        lat: this.coordinate[0],
        lng: this.coordinate[1],
      },
      icon: this.map.icons.red,
      pan: false,
      draggable: true,
      history: false,
    });
    this.onSetMarker(marker)
    // console.log(marker, marker._latlng);
  }

  showMarker() {
    this.map.markReverseGeocode({
      state: {
        latlng: {
          lat: this.coordinate[0],
          lng: this.coordinate[1],
        },
        zoom: 16,
      },
    });
  }

  initMap() {
    var map = new Mapp({
      element: '#app',
      presets: {
        latlng: {
          lat: this.coordinate[0],
          lng: this.coordinate[1],
        },
        zoom: 16,
      },
      apiKey: environment.apiKey,
    });
    map.addLayers();
    this.map = map;
  }

  onSetMarker(marker: any) {
    this.setMarker.emit(marker);
  }

  onSetCordinate(coordinate: Coordinate) {
    this.setCordinate.emit(coordinate);
  }
}
