import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AddressesService } from 'src/app/modules/shared-addresses/services/addresses.service';
import { Observable } from 'rxjs';
import { Address } from '../../models/address.model';
import { Coordinate } from '../../models/coordinate.model';

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.scss'],
})
export class AddLocationComponent implements OnInit {
  addressForm: FormGroup;
  editMode: boolean;
  isFavorite: boolean;
  id: number;
  marker: any;
  address: Address;
  isFetching = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private addressService: AddressesService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const { id } = params;
      this.isFavorite = this.route.snapshot.data.isFavorite;
      this.editMode = !!id;
      this.id = +id;
      this.initForm();
    });
  }

  initForm() {
    this.addressForm = new FormGroup({
      name: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
    });

    if (this.editMode) {
      this.fetchAddress();
    }
  }

  fetchAddress() {
    this.isFetching = true;
    const obs = this.isFavorite
      ? this.addressService.fetchFavoriteAddressById(this.id)
      : this.addressService.fetchAddressById(this.id);
    obs.subscribe(
      (address) => {
        this.address = address;
        this.isFetching = false;
        this.addressForm.setValue({
          name: address.name,
          address: address.address,
        });
      },
      () => (this.isFetching = false)
    );
  }

  onSubmit() {
    let obs: Observable<Address>;
    const address: Address = {
      ...this.addressForm.value,
      id: this.id,
      latitude: this.marker._latlng.lat,
      longiture: this.marker._latlng.lng,
    };
    if (this.editMode) {
      obs = this.isFavorite
        ? this.addressService.updateFavoriteAddress(address)
        : this.addressService.updateAddress(address);
    } else obs = this.addressService.addAddress(address);

    obs.subscribe(() => {
      this.router.navigate(['../'], { relativeTo: this.route });
    });
  }

  onSetMarker(marker: any) {
    this.marker = marker;
  }
}
