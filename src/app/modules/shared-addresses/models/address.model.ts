export interface Address {
  id: number;
  name: string;
  address: string;
  latitude: number;
  longiture: number;
  favoriteId?: number;
}
