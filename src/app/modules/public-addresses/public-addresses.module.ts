import { NgModule } from '@angular/core';
import { PublicAddressesRoutingModule } from './public-addresses-routing.module';
import { SharedAddressesModule } from '../shared-addresses/shared-addresses.module';
import { PublicAddressesComponent } from './components/public-addresses/public-addresses.component';

@NgModule({
  declarations: [PublicAddressesComponent],
  imports: [SharedAddressesModule, PublicAddressesRoutingModule],
})
export class PublicAddressesModule {}
