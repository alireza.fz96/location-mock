import { RouterModule, Route } from '@angular/router';
import { NgModule } from '@angular/core';

import { PublicAddressesComponent } from './components/public-addresses/public-addresses.component';
import { AddressDetailsComponent } from '../shared-addresses/components/address-details/address-details.component';
import { AddressStartComponent } from '../shared-addresses/components/address-start/address-start.component';
import { AddLocationComponent } from '../shared-addresses/components/add-location/add-location.component';

const routes: Route[] = [
  {
    path: '',
    component: PublicAddressesComponent,
    children: [
      { path: 'add', component: AddLocationComponent },
      {
        path: ':id',
        component: AddressDetailsComponent,
        data: { isFavorite: false },
      },
      { path: ':id/edit', component: AddLocationComponent },
      { path: '', component: AddressStartComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicAddressesRoutingModule {}
