import { Component, OnInit, OnDestroy } from '@angular/core';
import { Address } from '../../../shared-addresses/models/address.model';
import { AddressesService } from '../../../shared-addresses/services/addresses.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-public-addresses',
  templateUrl: './public-addresses.component.html',
  styleUrls: ['./public-addresses.component.scss'],
})
export class PublicAddressesComponent implements OnInit, OnDestroy {
  addresses: Address[] = [];
  addressSub: Subscription;
  error = '';

  constructor(private addressesService: AddressesService) {}

  ngOnInit(): void {
    this.addressSub = this.addressesService.addressesChanged.subscribe(() =>
      this.fetchAddresses()
    );
    this.fetchAddresses();
  }

  fetchAddresses() {
    this.error = '';
    this.addressesService.fetchAddresses().subscribe(
      (addresses) => {
        this.addresses = addresses;
        if (!this.addresses.length) this.error = 'There is no address !';
      },
      (error) => {
        this.error = error;
      }
    );
  }

  ngOnDestroy() {
    this.addressSub.unsubscribe();
  }
}
