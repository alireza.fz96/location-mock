import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [SpinnerComponent],
  imports: [CommonModule, ReactiveFormsModule, NgbModule],
  exports: [CommonModule, ReactiveFormsModule, SpinnerComponent, NgbModule],
})
export class SharedModule {}
