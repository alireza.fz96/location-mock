import { NgModule } from '@angular/core';
import { SharedAddressesModule } from '../shared-addresses/shared-addresses.module';

import { FavoriteAddressesRoutingModule } from './favorite-addresses-routing.module';
import { FavoriteAddressesComponent } from './components/favorite-addresses/favorite-addresses.component';

@NgModule({
  declarations: [FavoriteAddressesComponent],
  imports: [SharedAddressesModule, FavoriteAddressesRoutingModule],
})
export class FavoriteAddressesModule {}
