import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavoriteAddressesComponent } from './components/favorite-addresses/favorite-addresses.component';
import { AddressDetailsComponent } from '../shared-addresses/components/address-details/address-details.component';
import { AddressStartComponent } from '../shared-addresses/components/address-start/address-start.component';
import { AddLocationComponent } from '../shared-addresses/components/add-location/add-location.component';

const routes: Routes = [
  {
    path: '',
    component: FavoriteAddressesComponent,
    children: [
      {
        path: ':id',
        component: AddressDetailsComponent,
        data: { isFavorite: true },
      },
      {
        path: ':id/edit',
        component: AddLocationComponent,
        data: { isFavorite: true },
      },
      { path: '', component: AddressStartComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoriteAddressesRoutingModule {}
