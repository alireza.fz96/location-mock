import { Component, OnInit, OnDestroy } from '@angular/core';
import { Address } from '../../../shared-addresses/models/address.model';
import { AddressesService } from 'src/app/modules/shared-addresses/services/addresses.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-favorite-addresses',
  templateUrl: './favorite-addresses.component.html',
  styleUrls: ['./favorite-addresses.component.scss'],
})
export class FavoriteAddressesComponent implements OnInit, OnDestroy {
  addresses: Address[] = [];
  addressSub: Subscription;
  error = '';

  constructor(private addressesService: AddressesService) {}

  ngOnInit(): void {
    this.addressSub = this.addressesService.addressesChanged.subscribe(() =>
      this.fetchAddresses()
    );
    this.fetchAddresses();
  }

  fetchAddresses() {
    this.error = '';
    this.addressesService.fetchFavoriteAddresses().subscribe(
      (favoriteAddresses) => {
        this.addresses = favoriteAddresses;
        if (!this.addresses.length)
          this.error = 'There is no favorite address !';
      },
      (error) => {
        this.error = error.message;
      }
    );
  }

  ngOnDestroy() {
    this.addressSub.unsubscribe();
  }
}
