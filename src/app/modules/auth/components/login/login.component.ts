import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  authForm: FormGroup;
  loginMode = true;
  error = '';
  editMode = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.editMode = this.route.snapshot.data.editMode;
    this.initForm();
  }

  changeMode() {
    this.loginMode = !this.loginMode;
  }

  initForm() {
    let email = '';

    if (this.editMode) {
      email = this.authService.user.getValue().email;
    }

    this.authForm = new FormGroup({
      email: new FormControl(email, [Validators.email, Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
    });
  }

  onSubmit() {
    const { email, password } = this.authForm.value;

    let obser: Observable<User>;

    if (this.editMode) {
      obser = this.authService.updateUser(email, password);
    } else
      obser = this.loginMode
        ? this.authService.login(email, password)
        : this.authService.signup(email, password);

    this.error = '';

    obser.subscribe(
      () => this.router.navigate(['/']),
      (error) => {
        this.error = error;
      }
    );
  }
}
