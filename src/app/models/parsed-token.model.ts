export interface ParsedToken {
  email: string;
  sub: string;
  exp: number;
  iat: string;
}
