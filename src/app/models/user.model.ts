export class User {
  constructor(
    public email: string,
    public id: string,
    private _token: string,
    public expirationDate: Date
  ) {}

  get token() {
    if (!this.expirationDate || Date.now() > this.expirationDate.getTime())
      return null;
    return this._token;
  }
}
