import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NavigationItem } from './navigation-item.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-side-drawer',
  templateUrl: './side-drawer.component.html',
  styleUrls: ['./side-drawer.component.scss'],
})
export class SideDrawerComponent implements OnInit {
  isAuthenticated: boolean;
  navigationItems: NavigationItem[] = [
    {
      text: 'Addresses',
      link: '/public-addresses',
      show: true,
      icon: '../../../assets/icons/map.svg',
    },
    {
      text: 'Favorites',
      link: '/favorite-addresses',
      show: true,
      icon: '../../../assets/icons/star.svg',
    },
    {
      text: 'Login',
      link: '/auth',
      show: true,
      icon: '../../../assets/icons/person.svg',
    },
  ];
  @Input() show: boolean;
  @Output() toggleDrawer = new EventEmitter<void>();

  constructor(private authService: AuthService) {
    this.authService.user.subscribe((user) => {
      this.isAuthenticated = !!user;
      this.toggleShow(['Login']);
    });
  }

  ngOnInit(): void {}

  toggleShow(navItems: string[]) {
    for (let navItem of navItems) {
      this.navigationItems.find(
        (navigationItem) => navigationItem.text === navItem
      ).show = !this.isAuthenticated;
    }
  }

  onToggleDrawer() {
    this.toggleDrawer.emit();
  }
}
