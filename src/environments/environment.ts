// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey:
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ1NjQyMTQyYzM3ZTM2MDg0YzJlNjY1ZWQ1NjVkYzY0NjlhNGY0OWExNTZkZGZmMDUwYjIyM2UzMGM0OGMwNTExZmNiOGU1ODE4MGUzMzMzIn0.eyJhdWQiOiIxMDU3MCIsImp0aSI6IjQ1NjQyMTQyYzM3ZTM2MDg0YzJlNjY1ZWQ1NjVkYzY0NjlhNGY0OWExNTZkZGZmMDUwYjIyM2UzMGM0OGMwNTExZmNiOGU1ODE4MGUzMzMzIiwiaWF0IjoxNTk4MTYyMzQ5LCJuYmYiOjE1OTgxNjIzNDksImV4cCI6MTYwMDg0NDM0OSwic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.It59k_HUS0FZuAFvhTw0SKazcaOQpRYDcn6gNRBWj_URRcDv6MWzK_D5bfuz_y-TdnhzntECtVo-I8lofWWr9teTKRp2QcgSue_cgRR2ACGDpqnVn1rYv-MeWfPSj64h52Zr0AhrTJZkh7mKezoYL7RDDn9348VHLOiuNm5rhrkhfpd1qpJyW1YaMDENo0FSW-XwICVlgon4FgpzI_AOAVhA1mrw9Wi397oqpQYRT74MneDe0lAQ6TE_QSemp-fNq5b__hrfYhAU1b9b5_S2P_85r-lgaCRXDwWaKCTldABN8L5-qMdEBlsKM_1uL7DW3W8_YINtdGCVzt88BK2Kzw',
  baseUrl: 'http://localhost:3000',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
